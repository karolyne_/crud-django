from django.urls import path
from products import views

urlpatterns = [
    path('', listar, name='listar'),
    path('criar/', criar, name='criar'),
    path('atualizar/<int:id>/', atualizar, name='atualizar'),
    path('deletar/<int:id>/', deletar, name='deletar'),
]