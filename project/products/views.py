from django.shortcuts import render, redirect
from .models import Product
from .forms import ProductForm

def listar(request):
    products = Product.object.all()
    return render(request, 'products.html', {'products': products})

def criar(request):
    form = ProductForm(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect('listar')

    return render(request, 'products-forms.html', {'form': form})

def atualizar(request, id):
    product = Product.objects.get(id=id)
    form = ProductForm(request.POST or None, instance=product)

    if form.is_valid():
        form.save()
        return redirect('listar')
    return render(request, 'products-forms.html', {'form': form, 'product': product})


def deletar():
    product = Product.objects.get(id=id)
    form = ProductForm(request.POST or None, instance=product)

    if request.method == 'POST':
        product.delete()
        return redirect('listar')
    return render(request, 'prod-delete.html', {'product': product})
# Create your views here.
